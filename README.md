# GameInfo View/Api Generator

Info dbs:
```
-> ItemDB ✔️
-> QuestDB
-> MobDB
-> Crafting db?
-> Map Viewer?
```

## Why the Transformation to static site?
Lets convert this to parse and generate a static api that only needs to be served as a static folder:
(to save on ram and processing power)


## API
```
|- items
    |- all.json         // contains everything in a big json api
    |- viewer.json      // small index file that contains only id and name 
    |- discord.json     // json object tailored to what the discord bot wants
    |- [item id].json   // information about the item
    |- [item id].png   // (dyed) item image/icon
|- mobs
    |- discord.json     // json object tailored to what the discord bot wants
    |- definitions.json // elements- and races-definition
    |- [mobs id].json   // information about the mob
    |- [mobs id].png   // (dyed) mob image
...
```


## How to use it

Requirements:
```
git
nodejs >= 12
python >= 3
```

0. run `npm install`
1. clone submodules (with `git submodule update --init --recursive`)
2. run `npm run build`
3. configure your nginx/ apache to serve the `output` dir:

Configure the `output/` folder like you would normaly:
- Open html files then index.html
- serve it at the root of your domain
- look for the nearest 404 page and serve that one.

Configure it to open json files in in `output/api`:
```
$uri, $uri.json
```

also rewrite `i.tmw2.org/$uri` to `$host/items/$uri` so old links and short links will still work.

## to update (including updating the client-&server-data)

```
npm run update
```

> you can also do `npm run update -- --minify` to minify the result at the cost of an increased build time.

## Test it on your Local machine
run this command:
```
npm run serve
```

> The package used for serving has it's quirks
- It doesn't behave exactly like the configuration described above.
(for example 404 files or open json when there is no ending in api directory) 

- http://localhost:5000/api/items/all.json
- http://localhost:5000/api/items/discord.json
- http://localhost:5000/api/items/viewer.json
- http://localhost:5000/api/items/544.json


- http://localhost:5000/items/544