module.exports = require('rc')('tmw2-gameinfo-api-generator', {
  api_docs: "https://gitlab.com/TMW2/web-it-team/gameinfo-api/blob/master/README.md",
  api_url: "/api/",
  wiki_path: "https://git.themanaworld.org/ml/Docs/wikis/Items",
  /** minify output (css and html) increases build time dramaticaly */
  minify: false,
  apiHostAddress: "https://info.tmw2.org/api"
});
