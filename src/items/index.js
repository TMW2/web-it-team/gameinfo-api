const db = require("./db");
const format = require("./format");

const { writeJson, copyFile, writeFile, readFile } = require("fs-extra");
const { join } = require("path");
const { writePage } = require("../lib");
const config = require('../config')

const { dyePNG, parseDyeString } = require("dyecmdjs")

async function load_data() {
    await db.refresh();

    if (db.isEmpty) {
        throw new Error("DB is empty!!! did you init the submodules?")
    }
}

function isDyeStringValid(dyestring, item) {
    try {
        parseDyeString(dyestring)
        return true
    } catch (error) {
        console.debug("item has dyestring that couldn't be parsed", item.id, item.dyeString)
        return false
    }
}

async function writeApi(out_dir) {
    await writeJson(join(out_dir, 'all.json'), format.all())
    await writeJson(join(out_dir, 'viewer.json'), format.viewer())
    await writeJson(join(out_dir, 'head.json'), format.head())
    await writeJson(join(out_dir, 'discord.json'), format.discord())

    const write_item = async (item) => {
        await writeJson(join(out_dir, `${item.id}.json`), format.item(item.id))

        const img_path = join(__dirname, '../../clientdata/graphics/items/', item.image)
        const img_dest = join(out_dir, `${item.id}.png`)
        // gm(`${item.id}.png`).crop(width, height, 0, 0) // <- Crop image
        if (item.dyeString && isDyeStringValid(item.dyeString, item)) {
            // dyecmd this items file
            const pngBytes = await readFile(img_path)
            await writeFile(img_dest, dyePNG(pngBytes, item.dyeString))
        } else {
            // just copy the file
            await copyFile(img_path, img_dest)
        }
    }

    await Promise.all(db.findAll().map((item) => write_item(item)))
}

/**
 * 
 * @param {string} out_dir 
 * @param {import('../mobs/db')} mob_db 
 */
async function writeViewer(out_dir, mob_db) {
    await writePage('viewer/main.ejs', join(out_dir, 'index.html'), {
        content_file: 'items.ejs',
        title: "Item DB",
        nav_selection: 1,
        data: format.viewer(),
        itemTypes: db.itemTypes,
        image_path_prefix: join(config.api_url, '/items/')
    })

    await writePage('viewer/main.ejs', join(out_dir, '404.html'), {
        content_file: 'item404.ejs',
        title: "404 - Item DB",
        nav_selection: 1
    })

    const write_item = async (item) => {
        /** @type {{mobName: string, mobId: number, chances: string[]}[]} */
        const mobs_that_drop_it = []
        mob_db.getMobsThatDropItem(item.aegisName)
         .forEach(drop => {
            const existing = mobs_that_drop_it.find(({mobId})=> mobId === drop.mobId)
            const percentChance = (drop.chance / 100) + "%"
            if(existing){
                existing.chances.push(percentChance)
            } else {
                mobs_that_drop_it.push({
                    mobName: mob_db.findOne(drop.mobId).name,
                    mobId: drop.mobId,
                    chances: [percentChance]
                })
            }
         })

        await writePage('viewer/main.ejs', join(out_dir, `${item.id}.html`), {
            content_file: 'item.ejs',
            title: `${item.name} - Item DB`,
            nav_selection: 1,
            item,
            mobs_that_drop_it,
            wiki_path: config.wiki_path,
            api_path: join(config.api_url, '/items/'),
            metaImage: join(config.apiHostAddress, '/items/', item.image || 'error.png').replace(/^(https?:)\//,"$1//"),
            metaDescription: `${item.description}\n`
                + `${item.effect || ''}\nWeight: ${item.weight}`
        })
    }
    await Promise.all(db.findAll().map((item) => write_item(item)))

    await copyFile(
        join(__dirname, '../../clientdata/graphics/items/error.png'),
        join(out_dir, 'error.png')
    )
}


module.exports = { load_data, writeApi, writeViewer }
