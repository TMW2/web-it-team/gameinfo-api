const { to_json } = require('xmljson');
const { readFile } = require("fs-extra");
const path = require("path");
const { parseFile } = require('libconfig-parser')
const { getFromFile } = require('libconfig-parser/src/parts/include')

async function parseItemDB() {
  const data = parseFile('item_db.conf', path.join(__dirname, '../..', '/serverdata/db/re'), getFromFile);

  if (!Array.isArray(data.item_db))
    throw Error("data is invalid: item_db list is missing")

  const db = {};
  data.item_db.forEach((data) => {
    db[data.Id.toString()] = Object.assign({}, data, { Id: undefined });
  });
  return db;
}
async function parseItemClientXML() {
  const plainrawdata = await readFile(path.join(__dirname, '../..', '/clientdata/items.xml'), { encoding: 'utf8' });


  function XMLtoJSON(xml) {
    return new Promise((resolve, reject) => {
      to_json(xml, (err, res) => {
        if (err) {
          return reject(err);
        }

        resolve(res);
      });
    });
  };

  const data = await XMLtoJSON(plainrawdata);
  const items = data.items.item;
  const db = {};

  for (const key in items) {
    if (items.hasOwnProperty(key) && items[key].$.id >= 0) { // remove hair cuts and races from the list
      const item = items[key];
      const imageParts = item.$.image.split("|");

      db[item.$.id.toString()] = {
        description: item.$.description,
        effect: item.$.effect,
        image: imageParts[0],
        dyeString: imageParts[1],
        maxFloorOffset: item.$.maxFloorOffset,
        name: item.$.name,
        type: item.$.type,
        weight: item.$.weight
      };
    }
  }

  return db;
}

module.exports = { parseItemDB, parseItemClientXML };
