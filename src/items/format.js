const db = require('./db');
const config = require('../config')
const {join} = require('path')

function item(id) {
    return db.findOne(id)
}

function all() {
    return db.findAll();
}

function viewer() {
    const items = db.findAll();
    return items.map((item) => ({
        id: item.id,
        name: item.name,
        type: item.type
    }))
}

function head() {
    const items = db.findAll();
    return items.map((item) => ({
        id: item.id,
        name: item.name,
        type: item.type,
        image: item.image,
    }))
}

const discordDescriptionRules = [
    ["description", "_$val_"],
    ["effect", "**$val**"],
    ["atk", "> **Attack DMG:** `$val`"],
    ["matk", "> **Mana Attack DMG:** `$val`"],
    ["def", "> **Defense:** `$val`"],
    ["range", "> **Range:** `$val`"],
    ["equipLv", "> **Required Level:** `$val`"],
    ["weight", "> **Weight:** `$val g`"]
]

function discord() {
    const items = db.findAll();
    const makeItemDescriptionCard = (item) => {
        const lines = []; // description lines

        for (let rule of discordDescriptionRules) {
            const val = item[rule[0]];

            if (null != val) {
                lines.push(rule[1].replace(/\$val/, val));
            }
        }
      
        return {
          description: lines.join("\n"),
          title: item.name,
          image: join(config.apiHostAddress, `/items/${item.id}.png`).replace(/^(https?:)\//,"$1//")
        }
    };

    const result = {};

    for (let item of items) {
        result[item.id] = makeItemDescriptionCard(item)
    }

    return result
}

module.exports = {
    item,
    all,
    head,
    viewer,
    discord
}