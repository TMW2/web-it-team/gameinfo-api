const Case = require("case");

const parser = require('./parser');

const _db = Symbol();

class ItemDataBase {
  constructor(data) {
    this[_db] = [];
  }

  findOne(id) {
    return this[_db].find((elem) => elem.id === id);
  }

  findOneByAegisName(aegisName) {
    return this[_db].find((elem) => elem.aegisName === aegisName);
  }

  /** 
   * @returns {{[key:string]:any}[]}
   */
  findAll() {
    return this[_db];
  }

  get isEmpty() {
    return this[_db].length === 0;
  }

  get itemTypes() {
    return this[_db]
      .map(({ type }) => type)
      .reduce((p, n) => p.includes(n) ? p : [...p, n], [])
  }

  async refresh() {

    const startTS = Date.now();
    const parse_step = await Promise.all([
      parser.parseItemDB()
      , parser.parseItemClientXML()
    ]);

    console.info(`parsing item db files took ${Date.now() - startTS} ms`);

    const server_data = parse_step[0];
    const client_data = parse_step[1];

    const uptodate_db = [];

    console.log("Validating Data...")
    for (let key in server_data) {
      const server_item = server_data[key];
      const client_item = client_data[key];

      const mixed_item = Object.assign({}, client_item, server_item);

      for (let property in mixed_item) {
        const value = mixed_item[property];

        delete mixed_item[property];

        property = Case.camel(property);

        mixed_item[property] = value;
      }

      mixed_item.image = mixed_item.image || 'error.png'

      mixed_item.id = +key;

      // TODO: Use here some lib(for example object-model)
      const isItemValid = (item) => {
        let incorrect = false;

        if (false == ("id" in item)) {
          console.error("[err] Item with invalid id");
          incorrect = true;
        }

        if (false == ("name" in item)) {
          console.error(`[err] No name for item ${item.id} found!`);
          incorrect = true;
        }

        if (false == ("description" in item)) {
          console.error(`[err] No description for item ${item.id} found!`);
          incorrect = true;
        }

        return !incorrect;
      }

      if (true == isItemValid(mixed_item)) {
        uptodate_db.push(mixed_item);
      }
    }

    this[_db] = uptodate_db;
  }
};

module.exports = new ItemDataBase();