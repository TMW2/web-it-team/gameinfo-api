//@ts-check
const { emptyDir, copyFile, writeFile } = require("fs-extra");
const { join } = require("path");
const sass = require('sass');
const { promisify } = require('util')
const { directory, writePage } = require("./lib");
const items = require("./items");
const mobs = require("./mobs")
const config = require('./config')

async function start() {
  const startTS = Date.now();

  // Load Data
  await items.load_data()
  await mobs.load_data()

  // Write
  const output_dir = await directory(__dirname, '../output')
  await emptyDir(output_dir)

  // API
  const api_dir = await directory(output_dir, 'api')
  await writePage('api/home.ejs', join(api_dir, 'index.html'), { config })
  await writePage('api/404.ejs', join(api_dir, '404.html'), { config })

  // Viewer
  await writePage('viewer/main.ejs', join(output_dir, 'index.html'), {
    content_file: 'home.ejs',
    title: "Gameinfo DB",
    nav_selection: 0,
    data: {},
    image_path_prefix: join(config.api_url, '/items/'),
    apiPathPrefix: config.api_url
  })
  // Viewer Assets
  // await copyFile(join(__dirname, '../assets/logo.png'), join(output_dir, 'logo.png'))
  const ScssResult = await promisify(sass.render)({
    file: join(__dirname, '../assets/main.scss'),
    outputStyle: config.minify ? 'compressed' : 'expanded'
  })

  await copyFile(join(__dirname, '../node_modules/normalize.css/normalize.css'), join(output_dir, 'normalize.css'))
  await copyFile(join(__dirname, '../assets/favicon.ico'), join(output_dir, 'favicon.ico'))
  
  await writeFile(
    join(output_dir, 'main.css'),
    ScssResult.css
  )

  // Items
  await items.writeApi(await directory(api_dir, 'items'))
  await copyFile(join(api_dir, '404.html'), join(api_dir, 'items', '404.html'))
  await items.writeViewer(await directory(output_dir, 'items'), mobs.db)

  // Mobs
  await mobs.writeApi(await directory(api_dir, 'mobs'))
  await mobs.writeViewer(await directory(output_dir, 'mobs'))

  console.info(`Done, Generation took ${Date.now() - startTS} ms`);
}

start();
