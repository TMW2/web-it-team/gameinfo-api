#! /usr/bin/env python3
# -*- coding: utf8 -*-
#
# Copyright (C) 2010-2011  Evol Online
# Copyright (C) 2018  TMW-2
# Author: Andrei Karas (4144)
# Author: Jesusalva
# Author: LawnCable
#
# based on redeign.py
# To get the mob drops to json
# Problem: 
# the monsterdatabase doesn't comply to the libconfig specs and using groups for mobdrops which have duplicated keys,
# some parsers read groups as objecect like the spec defines it and duplicated keys result reassigning the values, so mob drops are lost.

import datetime
import sys
import os
import json

Mobs=[]

class Mob:
  def __init__(self):
    self.id="0"
    self.drops=[]

def MobAlloc(ab):
    try:
        maab=int(ab.mobpt)
    except:
        maab=9901

    Mobs.append(ab)

def parseMobs():
    src=open(os.path.dirname(__file__) + "../../serverdata/db/re/mob_db.conf", "r", encoding="utf-8")

    start=False
    dropper=False
    skip=0
    x=Mob() # Only for pyflakes2

    for a2 in src:
        a=a2.replace('    ', '\t');
        if a == "{\n":
            if skip:
                skip=0
            if start:
                MobAlloc(x)
            else:
                start=True
            x=Mob()

        if skip:
            start=False
            x=Mob()
            continue
        if "	Id:" in a:
            x.id=stp(a)
        if x.id == "ID":
            continue
        elif 'Drops: ' in a:
            dropper=True
        elif dropper and '}' in a:
            dropper=False
        elif dropper:
            x.drops.append(stp(a).split(": "))
    # Write last entry
    MobAlloc(x)

    src.close()

def stp(x):
    return x.replace('\n', '').replace('|', '').replace('(int, defaults to ', '').replace(')', '').replace('basic experience', '').replace('"','').replace("    ","").replace("\t","").replace('(string', '').replace('Name: ','').replace('Element: ','').replace('Race: ','').replace('AttackDelay: ', '').replace('WalkMask: ','').replace('MoveSpeed: ', '').replace('AttackRange: ', '').replace('ViewRange: ','').replace('Attack: ','').replace('ViewRange: ','').replace('Hp: ','').replace('Id: ','').replace('Lv: ','').replace('view range','').replace('attack range','').replace('move speed','').replace('health','').replace('(int','').replace('attack delay','atk.').replace('(','').replace(')','').replace('WALK_','').replace('Def: ','').replace('Mdef: ','')

parseMobs()

drops=dict()

for mob in Mobs:
    mobdrops = []
    for drop in mob.drops:
        mobdrops.append(
            dict(
                itemAegisName= drop[0],
                chance= int(drop[1])
                )
            )
    drops[mob.id] = mobdrops

json.dump(drops,sys.stdout)

exit(0)
