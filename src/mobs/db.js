const Case = require("case");

const parser = require('./parser');

const _db = Symbol();

class MonsterDataBase {
  constructor(data) {
    this[_db] = [];
  }

  findOne(id) {
    return this[_db].find((elem) => elem.id === id);
  }

  /** 
   * @returns {{[key:string]:any}[]}
   */
  findAll() {
    return this[_db];
  }

  getMobsThatDropItem(ItemAegisName) {
    const result = [];
    for (const mob of this[_db]) {
        if (mob.drops && mob.drops.find(({itemAegisName})=> itemAegisName == ItemAegisName)){
            const drops = mob.drops.filter(({itemAegisName}) => itemAegisName == ItemAegisName)
            for (const {chance} of drops){
              result.push({
                mobId: mob.id,
                chance
              })
            }
        }
    }
    return result;
  }

  get isEmpty() {
    return this[_db].length === 0;
  }

  async refresh() {
    const startTS = Date.now();
    const parse_step = await Promise.all([
      parser.parseMobDB()
      , parser.parseMobClientXML(),
      parser.ParseDropsFromMobDB()
    ]);

    // todo load drops seperately

    console.info(`parsing mob db files took ${Date.now() - startTS} ms`);

    const server_data = parse_step[0];
    const client_data = parse_step[1];
    const server_data_drops = parse_step[2];

    const uptodate_db = [];

    console.log("Validating Data...")
    for (let key in server_data) {
      const server_mob = server_data[key];
      const client_mob = client_data[key];
      const server_mobdrops = server_data_drops[key];

      const mixed_mob = Object.assign({}, client_mob, server_mob);

      for (let property in mixed_mob) {
        const value = mixed_mob[property];
        delete mixed_mob[property];
        property = Case.camel(property);
        mixed_mob[property] = value;
      }

      delete mixed_mob.drops
      mixed_mob.drops = server_mobdrops || []

      mixed_mob.id = +key;

      // TODO: Use here some lib(for example object-model)
      const isItemValid = (mob) => {
        let incorrect = false;

        if (false == ("id" in mob)) {
          console.error("[err] Mob with invalid id");
          incorrect = true;
        }

        if (false == ("name" in mob)) {
          console.error(`[err] No name for mob ${mob.id} found!`);
          incorrect = true;
        }

        // if (false == ("description" in item)) {
        //   console.error(`[err] No description for item ${item.id} found!`);
        //   incorrect = true;
        // }

        return !incorrect;
      }

      if (true == isItemValid(mixed_mob)) {
        uptodate_db.push(mixed_mob);
      }
    }

    this[_db] = uptodate_db;
  }
};

module.exports = new MonsterDataBase();
