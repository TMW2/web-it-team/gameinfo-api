//@ts-check
const db = require("./db");
const format = require("./format");

const { writeJson, copyFile, writeFile, readFile } = require("fs-extra");
const { join } = require("path");
const { writePage } = require("../lib");
const config = require("../config");

const { dyePNG } = require("dyecmdjs");
const { composeImage, getDefaultSpriteState, getSpriteStates } = require("./compose_image");

async function load_data() {
  await db.refresh();

  if (db.isEmpty) {
    throw new Error("DB is empty!!! did you init the submodules?");
  }
}

async function writeApi(out_dir) {
  await writeJson(join(out_dir, 'all.json'), format.all())
  await writeJson(join(out_dir, 'discord.json'), format.discord())
  await writeJson(join(out_dir, 'definitions.json'), format.getDefinitions())

  for (const mob of db.findAll()) {
    await writeJson(join(out_dir, `${mob.id}.json`), format.mob(mob.id));
    try {
      if (!mob.sprites) {
        continue;
      }
      const defaultState = getDefaultSpriteState(await getSpriteStates(mob.sprites))
      const buffer = await composeImage(mob.sprites, defaultState.action, defaultState.direction, 0);
      await writeFile(join(out_dir, mob.id + ".png") , buffer);
    } catch (error) {
      console.log("Error with writing the image for" + mob.name, error);
    }
  }
}

async function writeViewer(out_dir) {
    await writePage('viewer/main.ejs', join(out_dir, 'index.html'), {
        content_file: 'mobs.ejs',
        title: "Mob DB",
        nav_selection: 2,
        data: format.viewer(),
        races: format.getDefinitions().races,
        image_path_prefix: join(config.api_url, '/mobs/')
    })

}

module.exports = {
  load_data,
  db,
  writeApi,
  writeViewer
};
