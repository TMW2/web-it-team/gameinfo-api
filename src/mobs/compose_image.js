//@ts-check
const { readFile } = require("fs-extra");
const xmljs = require("xml-js");
const { join } = require("path");
const { dye: dyeFn, parseDyeString } = require("dyecmdjs");
const sharp = require("sharp");

const ClientDataDir = join(__dirname, "../..", "clientdata/");

/**
 * @typedef {"down"|"left"|"up"|"right"|"all"|"default"} direction
 * @typedef {{type: "frame", index: number, delay: number, offsetX?: number, offsetY?: number} |
 * {type: "sequence", start: number, end: number, delay: number, repeat: number, offsetX?: number, offsetY?: number} |
 * {type: "jump", action:string }|
 * {type: "end"}
 * } AnimationPart
 * @typedef {{
 *   [direction:string]: AnimationPart[]
 * }} MobSpriteDefAnimations
 * @typedef {{
 *  imagesets: {[name: string]: {src: string, width: number, height: number, offsetX:number, offsetY:number}}
 *  actions: {[name: string]: {
 *      imageset: string,
 *      animations: MobSpriteDefAnimations
 *   }
 *  }
 * }} MobSpriteDef
 *  */

/**
 *
 * @param {string} path
 * @returns {Promise<MobSpriteDef>}
 */
async function parseMobSprite(path) {
  async function parseSpriteXml(file) {
    /** @type {MobSpriteDef["imagesets"]} */
    const imagesets = {};
    /** @type {MobSpriteDef["actions"]} */
    const actions = {};

    const plainrawdata = await readFile(join(ClientDataDir, file), "utf-8");
    // parse xml file
    const sprite = xmljs
      .xml2js(plainrawdata)
      .elements.find(elm => elm.name === "sprite");

    // console.log("readfile", path, sprite);

    for (const { type, name, attributes, elements } of sprite.elements) {
      if (type !== "element") {
        continue;
      }

      if (name === "imageset") {
        const { src, width, height, offsetX, offsetY } = attributes;
        imagesets[attributes.name] = {
          src,
          width: Number(width),
          height: Number(height),
          offsetX: Number(offsetX || 0),
          offsetY: Number(offsetY || 0)
        };
      } else if (name === "action") {
        const { name, imageset } = attributes;

        /** @type {MobSpriteDefAnimations} */
        const animations = {};

        for (const animation of elements || []) {
          if (animation.type !== "element" || animation.name !== "animation") {
            continue;
          }
          /** @type {AnimationPart[]} */
          const animationParts = [];

          for (const animElement of animation.elements) {
            if (animElement.type !== "element") {
              continue;
            }

            if (animElement.name === "end") {
              animationParts.push({ type: "end" });
              continue;
            }
            const {
              delay,
              offsetX,
              offsetY,
              repeat,
              start,
              end,
              index,
              action
            } = animElement.attributes;
            if (animElement.name === "frame") {
              animationParts.push({
                type: "frame",
                index,
                delay,
                offsetX,
                offsetY
              });
            } else if (animElement.name === "sequence") {
              animationParts.push({
                type: "sequence",
                start,
                end,
                repeat,
                delay,
                offsetX,
                offsetY
              });
            } else if (animElement.name === "jump") {
              animationParts.push({
                type: "jump",
                action
              });
            }
          }
          animations[
            (animation.attributes && animation.attributes.direction) || "all"
          ] = animationParts;
        }

        actions[name] = {
          imageset,
          animations
        };
      } else if (name === "include") {
        // (also include includes, but they are less important than the file that loaded them)
        // console.log("include", elm.attributes);
        const included = await parseSpriteXml(
          join("graphics/sprites", attributes.file)
        );

        for (const key in included.imagesets) {
          if (included.imagesets.hasOwnProperty(key)) {
            if (imagesets[key]) {
              // console.log("attempt to overwrite existing imageset:", key);
            } else {
              imagesets[key] = included.imagesets[key];
            }
          }
        }

        for (const key in included.actions) {
          if (included.actions.hasOwnProperty(key)) {
            if (actions[key]) {
              // console.log("attempt to overwrite existing action:", key);
            } else {
              actions[key] = included.actions[key];
            }
          }
        }
      }
    }

    return { imagesets, actions };
  }

  return parseSpriteXml(path);
}
/**
 * @param {{path, dyestring}[]} layers
 * @returns {Promise<{[key:string]: direction[]}>}
 */
async function getSpriteStates(layers) {
  const layersStates = [];
  for (const layer of layers) {
    /** @type {{[key:string]: string[]}} */
    const layerStates = {};
    const sprite = await parseMobSprite(join("graphics/sprites", layer.path));
    const actions = Object.keys(sprite.actions);
    for (const action of actions) {
      const directions = Object.keys(sprite.actions[action].animations);
      layerStates[action] = directions;
    }
    layersStates.push(layerStates);
  }
  //@ts-ignore
  return layersStates.reduce((previous, current) => {
    const base = { ...previous };
    for (const key of Object.keys(current)) {
      if (base[key]) {
        const directions = base[key];
        for (const direction of current[key]) {
          if (!directions.includes(direction)) {
            directions.push(direction);
          }
        }
        base[key] = directions;
      } else {
        base[key] = current[key];
      }
    }
    return base;
  });
}

/** @type {direction[]} */
const defaultDirections = ["down", "all", "default"];
/**
 * @param {{[key:string]: direction[]}} spriteStates
 */
function getDefaultSpriteState(spriteStates) {
  if (spriteStates["stand"]) {
    const direction = defaultDirections.find(dir =>
      spriteStates["stand"].includes(dir)
    );
    if (!direction) {
      throw new Error("No default direction found");
    }
    return { action: "stand", direction };
  } else {
    const action = Object.keys(spriteStates)[0];
    return { action, direction: spriteStates[action][0] };
  }
}

const canvasSize = 256;
const padding = 5;
const mapTileSize = 32;
/**
 *
 * @param {{path, dyestring}[]} layers
 * @param {string} action
 * @param {direction} direction
 * @param {number} time currently unused
 *
 * @returns {Promise<Buffer>} Buffer containing the resulting png
 */
async function composeImage(layers, action, direction, time) {
  if (time) {
    console.log("times other than time=0 are not supported yet");
    return;
  }
  /** @type {sharp.Sharp} */
  let img = sharp({
    create: {
      width: 256,
      height: 256,
      channels: 4,
      background: { r: 0, g: 0, b: 0, alpha: 0 }
    }
  }).png();
  //   let baseHeight = 0,
  //     baseWidth = 0;
  /** @type {sharp.OverlayOptions[]} */
  const compositeArray = [];
  for (const layer of layers) {
    // load sprite
    const sprite = await parseMobSprite(join("graphics/sprites", layer.path));
    const { image, offsetX, offsetY, width, height } = await renderLayer(
      sprite,
      layer.dyestring,
      action,
      direction,
      time
    );
    // biggest layer gives the size
    // baseHeight = Math.max(height, baseHeight);
    // baseWidth = Math.max(width, baseWidth);

    // add layer to image
    let top, left;
    if (offsetY || offsetX) {
      const oy = Number(offsetY || 0);
      const ox = Number(offsetX || 0);

      top = Math.round(canvasSize / 2 - height / 2 + oy);
      left = Math.round(canvasSize / 2 - width / 2 + ox);
    }
    //   console.log({ top, left });
    compositeArray.push({
      input: await image.toBuffer(),
      top,
      left,
      blend: "over"
    });
  }

  return await sharp(await img.composite(compositeArray).toBuffer())
    // .extract({
    //   top: Math.round(canvasSize / 2 - baseHeight / 2),
    //   left: Math.round(canvasSize / 2 - baseWidth / 2),
    //   height: baseHeight,
    //   width: baseWidth
    // })
    .trim(0.1)
    .extend({
      top: padding,
      bottom: padding,
      right: padding,
      left: padding,
      background: { r: 0, g: 0, b: 0, alpha: 0 }
    })
    .toFormat("png")
    .toBuffer();
}

/**
 *
 * @param {MobSpriteDef} sprite
 * @param {any} dyestring
 * @param {string} action
 * @param {direction} direction
 * @param {number} time
 */
async function renderLayer(sprite, dyestring, action, direction, time) {
  //   console.log({ sprite });
  const act = sprite.actions[action];
  if (!act) {
    throw new Error("Action not found: " + action);
  }
  const imageset = sprite.imagesets[act.imageset];
  if (!imageset) {
    throw new Error("Imageset not found: " + act.imageset);
  }

  // locate image file and second part of dyestring
  const [filepath, dye] = imageset.src.split("|");
  let image = sharp(join(ClientDataDir, filepath));
  if (dyestring) {
    // handle dying the image
    const dyeParts1 = dye.split(";");
    const dyeParts2 = dyestring.split(";");
    const resultingDyestring = dyeParts1
      .map((d, i) => d + ":" + dyeParts2[i])
      .join(";");
    // console.log(filepath, dyeParts1, dyeParts2)
    try {
      // parseDyeString(resultingDyestring) // throws when it fails, so image object isn't modified
      const { data, info } = await image.clone()
        .raw()
        .toBuffer({ resolveWithObject: true });
      const { width, height, channels } = info;
      image = sharp(
        Buffer.from(
          dyeFn(
            // @ts-ignore
            new Uint8Array(data),
            resultingDyestring
          )
        ),
        {
          raw: {
            width: width,
            height: height,
            // @ts-ignore
            channels: channels
          }
        }
      ).png();
    } catch (error) {
      console.log(filepath, ":", dye, dyestring);
      console.log(error);
    }
  }

  // through the xml find out what we really need for the pose
  const animation = act.animations[direction] || act.animations["all"];
  if (!animation) {
    throw new Error("animation direction not found: " + direction);
  }

  const animationPart = animation[0]; // TODO implement that we get the right element for time x

  let index, offsetX, offsetY;

  if (animationPart.type === "frame") {
    index = Number(animationPart.index);
    offsetX = animationPart.offsetX;
    offsetY = animationPart.offsetY;
  } else if (animationPart.type === "sequence") {
    index = Number(animationPart.start); // TODO implement time so we get right index for the time we are at
    offsetX = animationPart.offsetX;
    offsetY = animationPart.offsetY;
  } else {
    throw new Error("Not Implemented: animationPart.type" + animationPart.type);
  }

  // crop the layer to what we need
  const { width: imageWidth, height: imageHeight } = await image.metadata();

  const inOneRow = imageWidth / imageset.width;
  const inOneColumn = imageHeight / imageset.height;

  const row = (index - (index % inOneRow)) / inOneRow;
  const column = index % inOneRow;

  // console.log({ inOneRow, inOneColumn, index, row, column });
  // console.log(filepath, offsetX, offsetY);

  image.extract({
    left: column * imageset.width,
    top: row * imageset.height,
    width: imageset.width,
    height: imageset.height
  });

  // TODO what about imageset offset, where does it need to be applied?

  return {
    image,
    offsetX,
    offsetY,
    width: imageset.width,
    height: imageset.height
  };
}

module.exports = {
  parseMobSprite,
  composeImage,
  getSpriteStates,
  getDefaultSpriteState
};

// TODO
// also include imageset offset?
