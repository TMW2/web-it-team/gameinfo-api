const { to_json } = require('xmljson');
const fs = require("fs-extra");
const path = require("path");
const { parseFile } = require('libconfig-parser')
const { getFromFile } = require('libconfig-parser/src/parts/include')
const {spawn} = require('child_process')

async function parseMobDB() {
  const data = parseFile('mob_db.conf', path.join(__dirname, '../..', '/serverdata/db/re'), getFromFile);

  if (!Array.isArray(data.mob_db))
    throw Error("data is invalid: mob_db list is missing")

  const db = {};
  data.mob_db.forEach((data) => {
    db[data.Id.toString()] = Object.assign({}, data, { Id: undefined });
  });
  return db;
}

async function parseMobClientXML() {
  const plainrawdata = await fs.readFile(path.join(__dirname, '../..', '/clientdata/monsters.xml'), 'utf-8');

  function XMLtoJSON(xml) {
    return new Promise((resolve, reject) => {
      to_json(xml, (err, res) => {
        if (err) {
          return reject(err);
        }
        resolve(res);
      });
    });
  };

  const parseSprites = (obj) => {
    let sprites = typeof obj === "string" ?
     [obj] : Object.keys(obj).map(key => obj[key]);
    return sprites.map(sprite => {
      const parts = sprite.split('|')
      return {
        path: parts[0],
        dyestring: parts[1]
      }
    })
  }

  const parseSounds = (obj) => {
    const sounds =  obj._ && obj.$ ?/* if only one item */
        [obj] : Object.keys(obj).map(key => obj[key]);
    const soundBank = {}
    for (const sound of sounds) {
      if(!soundBank[sound.$.event]){
        soundBank[sound.$.event] = []
      }
      soundBank[sound.$.event].push(sound._)
    }
    return soundBank
  }

  const data = await XMLtoJSON(plainrawdata);
  const monsters = data.monsters.monster;
  const db = {};

  for (const key in monsters) {
    const mob = monsters[key];

    const {
        name,
        targetCursor,
        walkType,
        sortOffsetY,
        targetOffsetY
    } = mob.$;

    db[mob.$.id.toString()] = {
        name,
        targetCursor,
        walkType,
        sortOffsetY,
        targetOffsetY,

        sprites: parseSprites(mob.sprite || {}), // format {path,dyestring}[]
        sounds: parseSounds(mob.sound || {}), // todo:{[event]:path[]}
        //attacks:, // TODO, irrelevant for now as this look like only being used for missile sprites
        particlefx: parseSprites(mob.particlefx || {}),
    };  
  }

  return db;
}

async function ParseDropsFromMobDB () {
  const reader = new Promise((res,rej)=>{
    const python = spawn('python3', ['parse_drops.py'], {cwd: __dirname});
    let data ="";
    python.stdout.on('data', (data_part) => {
      data += data_part
    });
  
    python.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });
  
    python.on('close', (code) => {
      if (code !== 0){
        rej(new Error(`python process exited with code ${code}`))
      } else {
        res(data)
      }
      console.log(`python process exited with code ${code}`);
    });
  })

  return JSON.parse(await reader)
}

module.exports = { parseMobDB, parseMobClientXML,ParseDropsFromMobDB };
