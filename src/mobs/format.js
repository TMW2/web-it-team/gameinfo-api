//@ts-check
const db = require("./db");
const config = require("../config");
const { join } = require("path");

const racesDefinition = [
  "Formless", // 0
  "Undead", // 1
  "Brute", // 2
  "Plant", // 3
  "Insect", // 4
  "Mineral", // 5
  "-", // 6
  "SemiHuman", // 7
  "Angel", // 8
  "Dragon", // 9
  "Player", // 10
  "Boss", // 11
  "NonBoss", // 12
  "NonSemiHuman", // 14
  "NonPlayer", // 15
  "SemiPlayer", // 16
  "NonSemiPlayer" // 17
];

const elementDefinition = [
  ["Neutral", "#000"], // 0
  ["Water", "#00F"], // 1
  ["Earth", "#7A0"], // 2
  ["Fire", "#F00"], // 3
  ["Wind", "#093"], // 4
  ["Nature", "#040"], // 5
  ["Holy", "#afa"], // 6
  ["Dark", "#908"], // 7
  ["Ghost", "#404"], // 8
  ["Undead", "#440"] // 9
];

function getDefinitions() {
  const elements = {},
    races = {};
  for (let i = 0; i < elementDefinition.length; i++) {
    elements[i] = elementDefinition[i];
  }
  for (let i = 0; i < racesDefinition.length; i++) {
    races[i] = racesDefinition[i];
  }
  return {
    elements,
    races
  };
}

function getElement(elementId) {
  if (elementDefinition[elementId]) {
    return elementDefinition[elementId];
  } else {
    throw new Error("ERROR, INVALID ELEM ID:" + elementId);
  }
}

/**
 * 
 * @param {[number, number]} elementDefinition 
 */
function parseElement(elementDefinition){
  const definiton =  getElement(elementDefinition[0])
  return {
    name: definiton[0],
    color: definiton[1],
    level: elementDefinition[1]
  }
}

function raceToString(raceId) {
  if (racesDefinition[raceId]) {
    return racesDefinition[raceId];
  } else {
    throw new Error("ERROR, INVALID RACE ID: " + raceId);
  }
}

function mob(id) {
  return db.findOne(id);
}

function all() {
  return db.findAll();
}

const discordDescriptionRules = [
  ["id", "> **ID:** **$val**"],
  ["lv", "> **Level:** **$val**"],
  ["Race", "> **Race:** **$val**"],
  ["elemental", "> **Elemental:** **$val**"],
  ["hp", "> **HP:** **$val**"],
  ["def", "> **DEF:** **$val**"],
  ["mdef", "> **M DEF:** **$val**"],
  ["exp", "> **EXP:** **$val**"],
  ["jExp", "> **JOB EXP:** **$val**"],
  ["Aggressive", "> **Aggressive:** **$val**"]
];

function discord() {
  const items = db.findAll();
  const makeItemDescriptionCard = item => {
    const lines = []; // description lines

    for (let rule of discordDescriptionRules) {
      const val = item[rule[0]];

      if (null != val) {
        lines.push(rule[1].replace(/\$val/, val));
      } else if (rule[0] == "elemental") {
        const {name,level} = parseElement(item["element"]);
        const val = `${name} LEVEL ${level}`
        lines.push(rule[1].replace(/\$val/, val));
      } else if (rule[0] == "Race") {
        const val = raceToString(item["race"]);
        lines.push(rule[1].replace(/\$val/, val));
      }
    }

    if (item.drops) {
      lines.push("**Drops**:```");
      for (const dropChance of item.drops) {
        lines.push(`${dropChance.itemAegisName} ${dropChance.chance / 100}%`);
      }
      lines.push("```");
    }

    return {
      description: lines.join("\n"),
      title: item.name,
      image: join(config.apiHostAddress, `/mobs/${item.id}.png`).replace(
        /^(https?:)\//,
        "$1//"
      )
    };
  };

  const result = {};

  for (let item of items) {
    result[item.id] = makeItemDescriptionCard(item);
  }

  return result;
}

function viewer() {
  return db.findAll().map(mob => ({
    id: mob.id,
    name: mob.name,
    race: mob.race,
    lv: mob.lv
  }));
}

module.exports = {
  mob,
  all,
  // head,
  viewer,
  discord,
  getDefinitions
};
