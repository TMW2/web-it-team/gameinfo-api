const { ensureDir, writeFile } = require("fs-extra");
const { join } = require("path");
const { renderFile } = require('ejs');
const minify = require('html-minifier').minify;
const config = require('./config')

async function directory(...args) {
  const dir = join(...args);
  await ensureDir(dir);
  return dir;
}
exports.directory = directory;

function mayMinifyHtml(html) {
  return config.minify ?
    minify(html, {
      collapseWhitespace: true,
      conservativeCollapse: true
    })
    : html
}

async function writeHTML(templatefile, destination, data) {
  const content = await renderFile(templatefile, data, {});
  await writeFile(destination, mayMinifyHtml(content));
}

async function writePage(template, destination, data) {
  const absolute_template_path = join(__dirname, 'views', template)
  await writeHTML(absolute_template_path, destination, data)
}
exports.writePage = writePage;